/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import util.ModelView;
import annotation.*;
import java.util.*;


/**
 *
 * @author Safidy
 */
@ClassAnnotation(map="test")
public class Departement {
    
    int id;
    String nomDept;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    

    public String getNomDept() {
        return nomDept;
    }
 

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }

   
    
    @UrlAnnotation(url="liste")
    public ModelView getAllDept()
    {
        ModelView retour=new ModelView();
        HashMap<String,Object> data=new HashMap();
        ArrayList<String> dept=new ArrayList();
        dept.add("Numerique");
        dept.add("Finance");
        dept.add("RH");
        data.put("liste", dept);
        retour.setData(data);
        retour.setUrl("listdept.jsp");
        return retour;
    }
    @UrlAnnotation(url="ajouter")
    public ModelView ajouter()
    {
         ModelView retour=new ModelView();
        HashMap<String,Object> data=new HashMap();
        data.put("obj", this);
        retour.setData(data);
        retour.setUrl("ajout.jsp");
        return retour;
    }
}
